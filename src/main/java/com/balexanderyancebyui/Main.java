package com.balexanderyancebyui;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Main {
    public static void main(String[] args) {

        try {

            //Create connection object
            HttpURLConnection con = createConnection();

            //Use the getCardFromAPI method to extract
            //json data from a web api
            JSONObject cardJSON = getCardFromAPI(con);

            //Create a Card object with these values
            // from the json object.
            Card card = new Card(
                    (String)cardJSON.get("suit"),
                    (String)cardJSON.get("value"));

            //Print the Card details to prove
            //It was actually created.
            System.out.println(String.format(
                    "Your card is the %s.", card.toString()));

        } catch (Exception e) { e.printStackTrace(); }
    }

    /********************************************************
     * Create a HttpURLConnection to the API. This also
     *    requires a Get request and a User-Agent.
     */
    static HttpURLConnection createConnection()
            throws Exception {

        //Pretend we have some sort of user agent.
        final String USER_AGENT = "Mozilla/5.0";


        //URL object
        // This URL gets a "new" deck,
        // shuffles it, and draws a card.
        URL url =new URL("https://deckofcardsapi.com/" +
                        "api/deck/new/draw/?count=1");

        //Create a connection and open it.
        HttpURLConnection con =
                (HttpURLConnection)url.openConnection();

        //Set the request method and request user-agent properties
        //Without these, the API will refuse to give us an input stream.
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);

        return con;
    }

    /********************************************************
     * Connect to the API with the HttpURLConnection object,
     *    grab a random card, return the JSONObejct.
     */
    static JSONObject getCardFromAPI(HttpURLConnection con)
            throws Exception {

        //Object
        JSONParser parser = new JSONParser();

        //Setup the Buffer for the incoming Stream from the API.
        BufferedReader in =
                new BufferedReader(
                        new InputStreamReader(
                                con.getInputStream()));

        //Create a string containing the value of the next line from
        // the BufferedReader since only one line will be passed,
        // no While loop will be necessary.
        String cardString = in.readLine();

        //Close the stream.
        in.close();

        //Create a JSON Object by parsing the cardString.
        JSONObject json = (JSONObject)parser.parse(cardString);

        //Now that we have the object, we need to get the hand
        // of cards dealt to us. It's only one card,
        // but we still need to grab the whole "array"
        JSONArray jsonArray = (JSONArray)json.get("cards");

        //Overwrite the previous JSONObject with the first
        // index of the array.
        json = (JSONObject)jsonArray.get(0);

        return json;
    }
}