package com.balexanderyancebyui;

public class Card {

    String suit;
    String value;

    public Card (String newSuit, String newValue) {
        suit = newSuit;
        value = newValue;
    }

    @Override
    public String toString() {
        return String.format("%s of %s", this.value, this.suit);
    }
}
